package Ferienaufgabe;

import FastSearch.FastSearch;
import Utils.Timer;

import java.util.Random;

/* Ergebnisse
Time Linear-Search average case: 1647
11499999
Time Fast-Search best case: 0
11500000
Time Linear-Search worst case: 3386
12000000
Time Fast-Search worst case: 0
12000000
Time Linear-Search: 2188
11750000
Time Fast-Search average case: 1
11750000
Time Linear-Search best case: 1
11000000
Time Fast-Search: 0
11000000
Time Linear-Search NIL case: 3218
-1
Time Fast-Search NIL case: 1
-1
*/

public class Ferienaufgabe_Oktober2018 {

    private static long[] array = new long[15000000];
    private static Random random = new Random();
    private static final int ANZAHL_DURCHGAENGE = 1000;

    static void generateList(int add) {
        array = new long[15000000 + add*1000000];
        array[0] = 2;
        for(int x = 1; x < array.length; x++) {
            long temp = random.nextInt(10);
            array[x] = array[x-1] + temp;
            //System.out.println("X: " + x + "\tValue: " + array[x]);
        }
    }

    public static void main(String[] args) {

        for (int y = 0; y < 5; y++) {
            System.out.println("Round " + y);
            generateList(y);

            //Fast-Search best case, Linear-Search average case
            Timer timer = new Timer();
            timer.start();
            int temp = -2;
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.linearSearch(0, array.length-1, array[array.length / 2], array);
            System.out.println("Time Linear-Search average case: " + timer.stop());
            System.out.println(temp);

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.fastSearch(0, array.length-1, array[array.length / 2], array);
            System.out.println("Time Fast-Search best case: " + timer.stop());
            System.out.println(temp);

            //Fast-Search worst case, Linear-Search worst case

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.linearSearch(0, array.length-1, array[array.length-1], array);
            System.out.println("Time Linear-Search worst case: " + timer.stop());
            System.out.println(temp);

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.fastSearch(0, array.length-1, array[array.length-1], array);
            System.out.println("Time Fast-Search worst case: " + timer.stop());
            System.out.println(temp);

            //Fast-Search average case, Linear-Search

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.linearSearch(0, array.length-1, array[array.length / 4 + array.length / 2], array);
            System.out.println("Time Linear-Search: " + timer.stop());
            System.out.println(temp);

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.fastSearch(0, array.length-1, array[array.length / 4 + array.length / 2], array);
            System.out.println("Time Fast-Search average case: " + timer.stop());
            System.out.println(temp);

            //Fast-Search, Linear-Search best case

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.linearSearch(0, array.length-1, array[array.length / 2], array);
            System.out.println("Time Linear-Search best case: " + timer.stop());
            System.out.println(temp);

            timer.start();
            for (int x = 0; x < ANZAHL_DURCHGAENGE; x++)
                temp = FastSearch.fastSearch(0, array.length-1, array[array.length / 2], array);
            System.out.println("Time Fast-Search: " + timer.stop());
            System.out.println(temp);
        }
    }
}
