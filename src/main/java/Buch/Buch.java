package Buch;

public class Buch implements Comparable<Buch> {
    private String ISBN;

    @Override
    public boolean equals(Object obj) {
        Buch buch = (Buch)obj;
        return buch.getISBN().equals(this.ISBN);
    }

    @Override
    public int compareTo(Buch buch) {
        return equals(buch) ? 1 : -1;
    }

    private String autor;
    private String titel;

    public Buch(String autor, String titel, String isbn) {
        this.autor = autor;
        this.titel = titel;
        this.ISBN = isbn;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setIsbn(String isbn) {
        this.ISBN = isbn;
    }


    @Override
    public String toString() {
        return this.titel + " " + this.autor + " " + this.ISBN;
    }
}
