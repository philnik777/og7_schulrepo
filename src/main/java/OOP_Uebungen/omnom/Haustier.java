package OOP_Uebungen.omnom;

import java.util.Random;

public class Haustier {
    private int futterLevel;
    private int muede;
    private int zufrieden;
    private int gesund;
    private String name;
    Random random = new Random();

    public Haustier(String name) {
        this.name = name;
        futterLevel = 100;
        muede = 100;
        zufrieden = 100;
        gesund = 100;
    }

    public int getHunger() {
        return futterLevel;
    }

    public void setHunger(int futterLevel) {
        if (futterLevel < 100 && futterLevel > 0) {
            this.futterLevel = futterLevel;
        }
        this.futterLevel = (futterLevel > 100) ? 0 : 100;
    }

    public int getMuede() {
        return muede;
    }

    public void setMuede(int muede) {
        if (muede < 100 && muede > 0) {
            this.muede = muede;
        }
        this.muede = (muede > 100) ? 0 : 100;
    }

    public int getZufrieden() {
        return zufrieden;
    }

    public void setZufrieden(int zufrieden) {
        if (zufrieden < 100 && zufrieden > 0) {
            this.zufrieden = zufrieden;
            return;
        }
        this.zufrieden = (zufrieden > 100) ? 0 : 100;
    }

    public void setGesund(int gesund) {
        if (gesund < 100 && gesund > 0) {
            this.gesund = gesund;
            return;
        }
        this.gesund = (gesund > 100) ? 0 : 100;
    }

    public int getGesund() {
        return gesund;
    }

    public String getName() {
        return name;
    }

    public void fuettern(int extraFutterWahrscheinlichkeit) {
        this.futterLevel += random.nextInt(100) % extraFutterWahrscheinlichkeit;
        if (this.futterLevel > 100) {this.futterLevel = 100;}
    }

    public void schlafen(int extraSchlafWahrscheinlichkeit) {
        this.muede += random.nextInt(100) % extraSchlafWahrscheinlichkeit;
        if (this.muede > 100) {this.muede = 100;}
    }

    public void spielen(int extraZufriedenWahrscheinlichkeit) {
        this.zufrieden += random.nextInt(100) % extraZufriedenWahrscheinlichkeit;
        if (this.zufrieden > 100) {this.zufrieden = 100;}
    }

    public void heilen() {
        this.gesund = 100;
    }
}
