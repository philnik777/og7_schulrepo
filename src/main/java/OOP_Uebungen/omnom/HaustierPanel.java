package OOP_Uebungen.omnom;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.JPanel;


public class HaustierPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private Image[] img;
	private boolean hunger;
	private boolean muede;
	private boolean zufrieden;
	private boolean gesund;
	private boolean essen;
		
	/**
	 * Create the panel.
	 */
	public HaustierPanel() {
		super();
		this.hunger = true;
		this.muede = true;
		this.zufrieden = true;
		this.gesund = true;
		this.essen = false;

		URL testPath = this.getClass().getResource("/OOP_Uebungen/omnom/normal.png");
		String testPath2 = testPath.getPath();
		img = new Image[10];
		img[0] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/normal.png").getPath());
		img[1] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/normal2.png").getPath());
		img[2] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/normal3.png").getPath());
		img[3] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/krank.png").getPath());
		img[4] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/hungrig.png").getPath());
		img[5] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/traurig.png").getPath());
		img[6] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/muede.png").getPath());
		img[7] = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/OOP_Uebungen/omnom/isst.png").getPath());
		
	}
	
	public void setHunger(boolean hunger) {
		this.hunger = hunger;
	}

	public void setMuede(boolean muede) {
		this.muede = muede;
	}

	public void setZufrieden(boolean zufrieden) {
		this.zufrieden = zufrieden;
	}

	public void setGesund(boolean gesund) {
		this.gesund = gesund;
	}
	
	public void setEssen(boolean essen){
		this.essen = essen;
	}

	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if(essen)
			g.drawImage(img[7], 1, 1, 89, 89, null);
		else if(hunger && muede && zufrieden && gesund){
			int bild = (int)(System.currentTimeMillis() / 5000 % 3);
		    g.drawImage(img[bild], 1, 1, 89, 89, null);
		}else if(!gesund){
			g.drawImage(img[3], 1, 1, 89, 89, null);
		}else if(!hunger){
			g.drawImage(img[4], 1, 1, 89, 89, null);
		}else if(!zufrieden){
			g.drawImage(img[5], 1, 1, 89, 89, null);
		}else if(!muede){
			g.drawImage(img[6], 1, 1, 89, 89, null);
		}else{
			g.drawImage(img[0], 1, 1, 89, 89, null);
		}
		
	}

}
