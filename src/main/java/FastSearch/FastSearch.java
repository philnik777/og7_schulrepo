package FastSearch;

public class FastSearch {

    public static final int ITEM_NOT_FOUND = -1;

    private static long array[] = {0, 100, 121, 234, 236, 355, 777, 789, 798, 852, 911, 935, 968, 1025};

    public static int linearSearch(int von, int bis, long x, long[] array) {
        for (int i = von; i < bis+1; i++) {
            if (array[i] == x) return i;
        }
        return ITEM_NOT_FOUND;
    }

    public static int fastSearch(int von, int bis, long x, long[] array) {
        if (von < bis) {
            int mb = (von+bis)/2;
            int mi = (int) (von + ((bis - von) * ((x-array[von])/(array[bis]-array[von]))));
            if (mb > mi) {
                int temp = mb;
                mb = mi;
                mi = temp;
            }
            if (x == array[mb])
                return mb;
            else if (x == array[mi])
                return mi;
            else if (x < array[mb])
                return fastSearch(von, mb-1, x, array);
            else if (x < array[mi])
                return fastSearch(mb+1, mi-1, x, array);
            else
                return fastSearch(mi+1, bis, x, array);
        }
        return ITEM_NOT_FOUND;
    }
}
