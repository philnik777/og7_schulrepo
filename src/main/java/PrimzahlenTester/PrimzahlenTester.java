package PrimzahlenTester;

import Utils.Timer;

public class PrimzahlenTester {
    private static final int OFFSET = 190800000;
    private static final int MAX_NUMBER = 200000000;
    private static Thread[] threads = new Thread[MAX_NUMBER];
    private static boolean[] isPrimeNumber = new boolean[MAX_NUMBER];
    public static boolean primzahlenTester(long zahl) {
        zahl = Math.abs(zahl);
        int x = 2;
        if (zahl < 2)
            return false;

        while (x < zahl) {
            if (zahl % x == 0)
                return false;
            x++;
        }
        return true;
    }

    public static void main(String[] args) {
        long dataArray[][] = new long[18][10];
        for (int y = 0; y < 10; y++) {
            long durchschnitt = 0;
            Timer timer = new Timer();
            int count = 0;
            for (int x = 0+ OFFSET; x < MAX_NUMBER + OFFSET; x+=740009) {
                timer.start();
                boolean isPrimeNumber = primzahlenTester(x);
                durchschnitt = (timer.stop() + ((x-1) * durchschnitt)) / x+1;
                if (isPrimeNumber) {
                    System.out.println("Time: " + timer.getTime() + "\t" + "Prime Number: " + x);
                    dataArray[count][y] = timer.getTime();
                    count++;
                }
            }
            System.out.println("Average Time: " + durchschnitt);
        }
        long averagedData[] = new long[18];
        for (int x = 0; x < dataArray.length; x++) {
            int temp = 0;
            for (long i : dataArray[x]) {
                temp += i;
            }
            System.out.println("Time: " + temp/dataArray[x].length);
        }
    }
}
