package Utils;

import java.util.Scanner;

public class ArrayHelper {
    public static String convertArrayToString(int[] zahlen) {
        String returnString = "[";
        for (int zahl : zahlen) {
            returnString += zahl + ", ";
        }
        returnString += "]";
        return returnString;
    }

    public static void aufgabe1() {
        int[] intArray = new int[20];
        for (int x = 0; x < intArray.length; x++) {
            System.out.println(intArray[x] = 1) ;
        }
    }

    public static void aufgabe2() {
        int[] intArray = new int[15];
        for (int i = 0; i < intArray.length; i++) {
            System.out.println(i);
        }
    }

    public static void aufgabe3() {
        int[] intArray = new int[17];
        int count = intArray.length-1;
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = count--;
        }
        System.out.println(convertArrayToString(intArray));
    }

    static Scanner scanner = new Scanner(System.in);
    public static void aufgabe4() {
        int[] intArray = new int[5];
        for (int i = 0; i < intArray.length; i++) {
            System.out.println("Zahl: ");
            intArray[i] = scanner.nextInt();
        }
        System.out.println(ArrayHelper.convertArrayToString(intArray));
    }

    public static void aufgabe5() {
        System.out.println("Zahl: ");
        int length = scanner.nextInt();
        int[] intArray = new int[length];
        for (int i = 0; i < intArray.length; i++) {
            intArray[i] = i;
        }
        System.out.println(ArrayHelper.convertArrayToString(intArray));
    }

    public static void aufgabe6() {
        System.out.println("Zahl1: ");
        int x = scanner.nextInt();
        System.out.println("Zahl2: ");
        int y = scanner.nextInt();
        int[][] array = new int[y][x];
        for (int n = 0; n < array.length; n++) {
            for (int m = 0; m < array[n].length; m++) {
                array[n][m] = m;
                System.out.print(array[n][m] + ", ");
            }
            System.out.println();
        }
    }

    public static void main(String args[]) {
        aufgabe1();
        aufgabe2();
        aufgabe3();
        aufgabe4();
        aufgabe5();
        aufgabe6();
    }
}
