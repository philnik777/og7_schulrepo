package Utils;

public class Timer {
    long startTime;
    long stopTime;

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public long stop() {
        stopTime = System.currentTimeMillis();
        return stopTime - startTime;
    }

    public long getTime() {
        return stopTime - startTime;
    }
}
