package OSZ_Kickers;

public class Spieler extends Person {
    private int trikotNr;
    private String position;

    public int getTrikotNr() {
        return trikotNr;
    }

    public void setTrikotNr(int trikotNr) {
        this.trikotNr = trikotNr;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
