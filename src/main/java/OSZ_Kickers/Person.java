package OSZ_Kickers;

public class Person {
    private String name;
    private String telNr;
    private boolean hatJahresbeitragBezahlt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelNr() {
        return telNr;
    }

    public void setTelNr(String telNr) {
        this.telNr = telNr;
    }

    public boolean hatJahresbeitragBezahlt() {
        return hatJahresbeitragBezahlt;
    }

    public void hatJahresbeitragBezahlt(boolean hatJahresbeitragBezahlt) {
        this.hatJahresbeitragBezahlt = hatJahresbeitragBezahlt;
    }
}
