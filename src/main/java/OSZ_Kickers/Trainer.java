package OSZ_Kickers;

public class Trainer extends Person {

    public enum LizenzKlasse {A, B, C}

    private LizenzKlasse lizenzKlasse;
    private int aufwandsEntschaedigung;

    public LizenzKlasse getLizenzKlasse() {
        return lizenzKlasse;
    }

    public void setLizenzKlasse(LizenzKlasse lizenzKlasse) {
        this.lizenzKlasse = lizenzKlasse;
    }

    public int getAufwandsEntschaedigung() {
        return aufwandsEntschaedigung;
    }

    public void setAufwandsEntschaedigung(int aufwandsEntschaedigung) {
        if (aufwandsEntschaedigung > 450 || aufwandsEntschaedigung < 125) return;
        this.aufwandsEntschaedigung = aufwandsEntschaedigung;
    }
}
