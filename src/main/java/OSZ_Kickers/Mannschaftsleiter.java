package OSZ_Kickers;

public class Mannschaftsleiter extends Spieler {
    private String mannschaftsName;
    private double rabatt;

    public String getMannschaftsName() {
        return mannschaftsName;
    }

    public void setMannschaftsName(String mannschaftsName) {
        this.mannschaftsName = mannschaftsName;
    }

    public double getRabatt() {
        return rabatt;
    }

    public void setRabatt(double rabatt) {
        this.rabatt = rabatt;
    }
}
