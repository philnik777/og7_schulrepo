package OSZ_Kickers;

public class Schiedsrichter extends Person {
    private int gepfiffeneSpiele;

    public int getGepfiffeneSpiele() {
        return gepfiffeneSpiele;
    }

    public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
        this.gepfiffeneSpiele = gepfiffeneSpiele;
    }

    public void neuesSpielGepfiffen() {
        gepfiffeneSpiele++;
    }
}
