package StarSim2099;


/**
 * Write a description of class Raumschiff here.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Raumschiff extends Position {
	private String typ;
	private String antrieb;
	private int maxLadekapazitaet;
	private int winkel;

	// Attribute
	
	// Methoden

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getTyp() {
		return typ;
	}

	public void setAntrieb(String antrieb) {
		this.antrieb = antrieb;
	}

	public String getAntrieb() {
		return antrieb;
	}

	public void setMaxLadekapazitaet(int maxLadekapazitaet) {
		this.maxLadekapazitaet = maxLadekapazitaet;
	}

	public int getMaxLadekapazitaet() {
		return maxLadekapazitaet;
	}

	public void setWinkel(int winkel) {
		this.winkel = winkel;
	}

	public int getWinkel() {
		return winkel;
	}
}
