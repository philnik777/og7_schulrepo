package OG_Vererbung.spaeti;

import java.util.List;

public class Lieferant {

	private String name;
	private String ort;
	private List<Artikel> lieferbareArtikel;

	public Lieferant() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}


    public List<Artikel> getLieferbareArtikel() {
        return lieferbareArtikel;
    }

    public void setLieferbareArtikel(List<Artikel> lieferbareArtikel) {
        this.lieferbareArtikel = lieferbareArtikel;
    }


}
