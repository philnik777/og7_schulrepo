package zahlensortierer;

import java.util.*;

/**
 * @author "Nikolas Klauser"
 */
public class Zahlensortierer {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		int[] zahlen = new int[3];
		for (int x = 0; x < 3; x++) {
		    System.out.println( x + ". Zahl: ");
		    zahlen[x] = scanner.nextInt();
        }

        for (int x = 1; x < 3; x++) {
            if (zahlen[x-1] > zahlen[x]) {
                int tmp = zahlen[x-1];
                zahlen[x-1] = zahlen[x];
                zahlen[x] = tmp;
            }
        }

        System.out.println("Smallest number: " + zahlen[0]);
        System.out.println("Largest number: " + zahlen[zahlen.length-1]);
		scanner.close();
	}
}
