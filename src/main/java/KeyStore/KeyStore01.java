package KeyStore;

public class KeyStore01 {
    private String[] strings;
    private int currentPos = 0;

    public KeyStore01() {
        strings = new String[100];
    }

    public KeyStore01(int length) {
        strings = new String[length];
    }

    public boolean add(String eintrag) {
        strings[currentPos] = eintrag;
        return true;
    }

    public int indexOf(String str) {
        for (int i = 0; i < strings.length; i++) {
            if (str.equals(strings[i])) return i;
        }
        return -1;
    }

    public boolean remove(int index) {
        if (index > strings.length-1) return false;
        strings[index] = null;
        return true;
    }

    public boolean remove(String str) {
        return remove(indexOf(str));
    }

    public void clear() {
        strings = new String[strings.length];
    }

    public String get(int index) {
        return strings[index];
    }

    public int size() {
        return strings.length;
    }

    @Override
    public String toString() {
        String string = new String();
        for (String s : strings) {
            if (s != null)
                string += (s + " ");
        }
        return string;
    }
}
