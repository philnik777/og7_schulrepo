package Tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 if (fuellstand + 5 < 100) {
				 fuellstand = fuellstand + 5;
				 f.myTank.setFuellstand(fuellstand);
			 } else {
			 	f.myTank.setFuellstand(100);
			 }

			 f.lblFuellstand.setText(""+fuellstand);
			 f.lblFuellstandProzent.setText(fuellstand+"%");
		}
		if (obj == f.btnVerbrauchen) {
		    double fuellstand = f.myTank.getFuellstand();
            fuellstand -= 2;
		    if (fuellstand < 0)
		        fuellstand = 0;
		    f.myTank.setFuellstand(fuellstand);
		    f.lblFuellstand.setText(""+fuellstand);
		    f.lblFuellstandProzent.setText(fuellstand+"%");
		}
        if (obj == f.btnResetTank) {
            f.myTank.setFuellstand(0);
            f.lblFuellstand.setText(""+f.myTank.getFuellstand());
			f.lblFuellstandProzent.setText(f.myTank.getFuellstand()+"%");
		}

	}
}