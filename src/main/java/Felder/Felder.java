package Felder;

import java.util.Arrays;

/**
  *
  * Übungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */

public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die größte Zahl der Liste zurückgeben
  public int maxElement(){
    int maxZahl = zahlenliste[0];
    for (int zahl : zahlenliste) maxZahl = zahl > maxZahl ? zahl : maxZahl;
    return maxZahl;
  }

  //die Methode soll die kleinste Zahl der Liste zurückgeben
  public int minElement(){
    int maxZahl = zahlenliste[0];
    for (int zahl : zahlenliste) maxZahl = zahl < maxZahl ? zahl : maxZahl;
    return maxZahl;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
  public int durchschnitt(){
    int gesamt = 0;
    for (int i : zahlenliste) {
      gesamt += i;
    }
    gesamt /= zahlenliste.length;
    return gesamt;
  }

  //die Methode soll die Anzahl der Elemente zurückgeben
  //der Befehl zahlenliste.length; könnte hierbei hilfreich sein
  public int anzahlElemente(){
    return zahlenliste.length;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){

      String returnString = "[";
      for (int zahl : zahlenliste) {
        returnString += zahl + ", ";
      }
      returnString += "]";
      return returnString;
  }

  //die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
    for (int i : zahlenliste) {
      if (i == zahl) return true;
    }
    return false;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
    for (int i = 0; i < zahlenliste.length; i++) {
      if (zahlenliste[i] == zahl) return i;
    }
    return 0;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)
  public void sortiere(){
    Arrays.sort(zahlenliste);
  }

  public static void main(String[] args) {
    Felder testenMeinerLoesung = new Felder();
    System.out.println(testenMeinerLoesung.maxElement());
    System.out.println(testenMeinerLoesung.minElement());
    System.out.println(testenMeinerLoesung.durchschnitt());
    System.out.println(testenMeinerLoesung.anzahlElemente());
    System.out.println(testenMeinerLoesung.toString());
    System.out.println(testenMeinerLoesung.istElement(9));
    System.out.println(testenMeinerLoesung.getErstePosition(5));
    testenMeinerLoesung.sortiere();
    System.out.println(testenMeinerLoesung.toString());
  }
}
