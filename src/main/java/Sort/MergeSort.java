package Sort;

import Utils.Timer;

public class MergeSort {
    static void sort(int arr[], int l, int r) {
        if (l < r) {
            int m = (l+r)/2;
            sort(arr, l, m);
            sort(arr , m+1, r);
            merge(arr, l, m, r);
        }
    }

    static void merge(int arr[], int l, int m, int r) {
        int x = m - l + 1;
        int y = r - m;

        int array[] = new int [x];
        int array2[] = new int [y];

        int debugArray1[] = new int[x];
        int debugArray2[] = new int[y];

        for (int i=0; i<x; ++i) {
            array[i] = arr[l + i];
            debugArray1[i] = l + i;
        }
        for (int j=0; j<y; ++j) {
            array2[j] = arr[m + 1 + j];
            debugArray2[j] = m + 1 + j;
        }

        int i = 0;
        int j = 0;
        int k = l;

        while (i < x && j < y) {
            if (array[i] <= array2[j]) {
                System.out.print("Move " + debugArray1[i] + " to " + k + " ");
                arr[k] = array[i];
                printArray(arr);
                i++;
            } else {
                System.out.print("Move " + debugArray2[j] + " to " + k + " ");
                arr[k] = array2[j];
                printArray(arr);
                j++;
            }
            k++;
        }
        while (i < x) {
            arr[k] = array[i];
            i++;
            k++;
        }
        while (j < y) {
            arr[k] = array2[j];
            j++;
            k++;
        }
    }

    private static void printArray(int[] array) {
        System.out.print("[ ");
        for (int i : array) {
            System.out.print(i + ", ");
        }
        System.out.println("]");
    }

    public static void main(String[] args) {
        Timer timer = new Timer();

        int[] banane = new int[] {5, 2, 4, 3, 20, 23, 52, 12, 25, 16, 70, 90, 110, 105};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Average case: " + timer.stop());

        banane = new int[] {2, 3, 4, 5, 12, 16, 20, 23, 25, 52, 70, 90, 105, 110};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Best case: " + timer.stop());

        banane = new int[] {110, 105, 90, 70, 52, 25, 23, 20, 16, 12, 5, 4, 3, 2};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Worst case: " + timer.stop());


        banane = new int[] {5, 2, 4, 3, 20, 23, 52, 12, 25, 16, 70};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Average case: " + timer.stop());

        banane = new int[] {2, 3, 4, 5, 12, 16, 20, 23, 25, 52, 70};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Best case: " + timer.stop());

        banane = new int[] {70, 52, 25, 23, 20, 16, 12, 5, 4, 3, 2};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Worst case: " + timer.stop());

        banane = new int[] {20, 23, 52, 12, 25, 16, 70};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Average case: " + timer.stop());

        banane = new int[] {12, 16, 20, 23, 25, 52, 70};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Best case: " + timer.stop());

        banane = new int[] {20, 16, 12, 5, 4, 3, 2};
        timer.start();
        sort(banane, 0, banane.length-1);
        printArray(banane);
        System.out.println("Worst case: " + timer.stop());
    }
}
