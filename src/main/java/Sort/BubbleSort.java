package Sort;

import Utils.Timer;

import java.util.Random;

public class BubbleSort {

    private static int RERUNS = 100000;

    public static int[] bubbleSort(int[] array) {
        int temp;
        for(int x=1; x<array.length; x++) {
            boolean isSorted = true;
            for(int y=0; y<array.length-x; y++) {
                if(array[y]>array[y+1]) {
                    isSorted = false;
                    temp=array[y];
                    array[y]=array[y+1];
                    array[y+1]=temp;
                }
            }
            if (isSorted) return array;
        }
        return array;
    }

    public static void printArray(int array[])
    {
        int n = array.length;
        for (int i=0; i<n; ++i)
            System.out.print(array[i]+" ");
        System.out.println();
    }

    static int[] generateList() {
        Random random = new Random();
        int[] array;
        array = new int[150];
        array[0] = 2;
        for(int x = 1; x < array.length; x++) {
            int temp = random.nextInt(10);
            array[x] = array[x-1] + temp;
            //System.out.println("X: " + x + "\tValue: " + array[x]);
        }
        return array;
    }

    public static void main(String[] args) {

        printArray(generateList());

        //worst case
        int[] printArray = new int[0];
        Timer timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {39, 37, 35, 33, 31, 29, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1};
            printArray = bubbleSort(array);
        }
        System.out.print("Worst case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        //average case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1, 39, 37, 35, 33, 31, 29, 27, 25, 23};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        ///best case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        //worst case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {51, 49, 47, 45, 43, 41, 39, 37, 35, 33, 31, 29, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1};
            printArray = bubbleSort(array);
        }
        System.out.print("Worst case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        //average case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {21, 19, 17, 15, 13, 11, 51, 49, 47, 45, 43, 41, 9, 7, 5, 3, 1, 39, 37, 35, 33, 31, 29, 27, 25, 23};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        ///best case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        //worst case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {29, 27, 25, 23, 21, 19, 17, 15, 13, 11, 9, 7, 5, 3, 1};
            printArray = bubbleSort(array);
        }
        System.out.print("Worst case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        //average case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {33, 9, 7, 5, 3, 1, 39, 37, 35, 33, 31, 29, 27, 25, 23};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);

        ///best case
        printArray = new int[0];
        timer = new Timer();
        timer.start();
        for (int x = 0; x < RERUNS; x++) {
            int[] array = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29};
            printArray = bubbleSort(array);
        }
        System.out.print("Average case: ");
        System.out.println(timer.stop());
        printArray(printArray);
    }
}
