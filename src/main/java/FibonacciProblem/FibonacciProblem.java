package FibonacciProblem;

public class FibonacciProblem {
    public static int fibbonaci(int monate) {
        if (monate <= 2)
            return 1;
        return  fibbonaci(monate-2) + fibbonaci(monate-1);
    }
}
