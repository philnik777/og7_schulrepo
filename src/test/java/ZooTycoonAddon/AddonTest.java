package ZooTycoonAddon;

import org.junit.Test;

public class AddonTest {

    @Test
    public void test() {
        Addon addon = new Addon();
        addon.setAmount(3);
        assert addon.getAmount() == 3;
        addon.setId(0);
        assert addon.getId() == 0;
        addon.setMaxAmount(100);
        assert addon.getMaxAmount() == 100;
        addon.setName("Futter");
        assert addon.getName().equals("Futter");
        addon.setPrice(0.99);
        assert addon.getPrice() == 0.99;
    }

}
