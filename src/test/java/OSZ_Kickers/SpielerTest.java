package OSZ_Kickers;

import org.junit.jupiter.api.Test;

class SpielerTest {

    @Test
    void spielerTest() {
        Spieler spieler = new Spieler();

        spieler.setTrikotNr(55);
        assert spieler.getTrikotNr() == 55;

        spieler.setPosition("eine Position");
        assert spieler.getPosition().equals("eine Position");
    }
}