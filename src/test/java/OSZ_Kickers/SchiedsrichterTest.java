package OSZ_Kickers;

import org.junit.jupiter.api.Test;

class SchiedsrichterTest {

    @Test
    void schiedsrichterTest() {
        Schiedsrichter schiedsrichter = new Schiedsrichter();

        schiedsrichter.setGepfiffeneSpiele(126);
        assert schiedsrichter.getGepfiffeneSpiele() == 126;

        schiedsrichter.neuesSpielGepfiffen();
        assert schiedsrichter.getGepfiffeneSpiele() == 127;
    }
}