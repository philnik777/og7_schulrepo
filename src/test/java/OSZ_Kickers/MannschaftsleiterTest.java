package OSZ_Kickers;

import org.junit.jupiter.api.Test;

class MannschaftsleiterTest {

    @Test
    void mannschaftsleiterTest() {
        Mannschaftsleiter mannschaftsleiter = new Mannschaftsleiter();

        mannschaftsleiter.setMannschaftsName("BananenMannschaft");
        assert mannschaftsleiter.getMannschaftsName().equals("BananenMannschaft");

        mannschaftsleiter.setRabatt(3.1415);
        assert mannschaftsleiter.getRabatt() == 3.1415;
    }
}