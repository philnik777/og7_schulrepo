package OSZ_Kickers;

import org.junit.jupiter.api.Test;

class TrainerTest {

    @Test
    void trainerTest() {
        Trainer trainer = new Trainer();

        trainer.setLizenzKlasse(Trainer.LizenzKlasse.A);
        assert trainer.getLizenzKlasse().equals(Trainer.LizenzKlasse.A);

        trainer.setAufwandsEntschaedigung(250);
        assert trainer.getAufwandsEntschaedigung() == 250;
        trainer.setAufwandsEntschaedigung(500);
        assert trainer.getAufwandsEntschaedigung() != 500;
    }
}