package OSZ_Kickers;

import org.junit.jupiter.api.Test;

class PersonTest {

    @Test
    void personTest() {
        Person person = new Person();

        person.setName("Banane");
        assert person.getName().equals("Banane");

        person.setTelNr("0049 1234 567890");
        assert person.getTelNr().equals("+49 1234 567890");

        person.hatJahresbeitragBezahlt(true);
        assert person.hatJahresbeitragBezahlt();

        person.hatJahresbeitragBezahlt(false);
        assert !person.hatJahresbeitragBezahlt();
    }

}