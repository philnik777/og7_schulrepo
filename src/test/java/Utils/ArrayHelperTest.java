package Utils;

import org.junit.jupiter.api.Test;

class ArrayHelperTest {

    @Test
    void test() {
        int[] banane = {1, 2, 3, 4};
        assert ArrayHelper.convertArrayToString(banane).equals("[1, 2, 3, 4, ]");
    }
}